package com.kshrd.hw03;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class AnimationActivity extends AppCompatActivity {

    ImageView round;
    final static String INTENT = "Intent";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation);
        round = findViewById(R.id.roundImg);

        Intent intent = getIntent();
        String anim = intent.getStringExtra(INTENT);
        assert anim != null;
        switch (anim) {
            case "fadeIn": {
                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
                round.startAnimation(animation);
                break;
            }
            case "fadeOut": {
                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out);
                round.startAnimation(animation);
                break;
            }
            case "zoom": {
                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom);
                round.startAnimation(animation);
                break;
            }
            case "rotate": {
                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate);
                round.startAnimation(animation);
                break;
            }
        }
    }

}