package com.kshrd.hw03;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public final static String MESSAGE = "message";
    public final static String INTENT = "Intent";
    ListView listView;
    Button save;
    ArrayList<String> arrayList;
    ArrayAdapter<String> arrayAdapter;
    String title;
    EditText desc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listView);
        arrayList = new ArrayList<>();
        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayList);
        listView.setAdapter(arrayAdapter);
        save = findViewById(R.id.btnSave);
    }

    public void onClickAdd(View v) {
        Intent intent = new Intent();
        intent.setClass(MainActivity.this, NoteAddActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            assert data != null;
            title = data.getStringExtra(MESSAGE);
            arrayList.add(title);
            arrayAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent i = new Intent(getApplicationContext(), AnimationActivity.class);
        switch (item.getItemId()) {
            case R.id.addNote:
//                Toast.makeText(this, "selected addNewNote", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, NoteAddActivity.class);
                startActivityForResult(intent, 1);
                return true;
            case R.id.removeAllNotes:
                Toast.makeText(this, "selected removeAllNotes", Toast.LENGTH_SHORT).show();

                return true;
            case R.id.fadeIn:
                i.putExtra(INTENT, "fadeIn");
                startActivity(i);
                return true;
            case R.id.fadeOut:
                i.putExtra(INTENT, "fadeOut");
                startActivity(i);
                return true;
            case R.id.zoom:
                i.putExtra(INTENT, "zoom");
                startActivity(i);
                return true;
            case R.id.rotate:
                i.putExtra(INTENT, "rotate");
                startActivity(i);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}